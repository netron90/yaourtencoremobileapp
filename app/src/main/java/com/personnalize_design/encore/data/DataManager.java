package com.personnalize_design.encore.data;

import com.personnalize_design.encore.data.db.DbHelper;
import com.personnalize_design.encore.data.network.ApiHelper;
import com.personnalize_design.encore.data.preferences.PreferenceHelper;

public interface DataManager extends DbHelper, ApiHelper, PreferenceHelper {
}
