package com.personnalize_design.encore.data.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;



public class AppPreferenceHelper implements PreferenceHelper {

    private SharedPreferences mPref;
    private Context context;
    private String fileName;

    @Inject
    public AppPreferenceHelper(Context context){
        mPref = context.getSharedPreferences("encore_pref", Context.MODE_PRIVATE);
    }

//    @Override
//    public boolean isUserHaveCoupon ( ) {
//
//        return mPref.getBoolean (USER_COUPON, false);
//    }
//
//    @Override
//    public void setUserCoupon ( boolean couponValue) {
//
//        mPref.edit ().putBoolean (USER_COUPON, couponValue).apply ();
//
//    }
//
//    @Override
//    public boolean isConfirmationCodeValid ( ) {
//        return mPref.getBoolean (CONFIRMATION_CODE_VALID, false);
//    }
//
//
//    @Override
//    public void setConfirmationCode (boolean confirmationValidValue ) {
//        mPref.edit ().putBoolean (CONFIRMATION_CODE_VALID, confirmationValidValue).apply ();
//
//    }
//
//    @Override
//    public String getConfirmationNumber ( ) {
//        return mPref.getString (CONFIRMATION_VALUE, "");
//    }
//
//    @Override
//    public void setConfirmationNumber ( String confirmationNumber ) {
//        mPref.edit ().putString (CONFIRMATION_VALUE, confirmationNumber).apply ();
//    }
//
//    @Override
//    public String getCodeSecret ( ) {
//        return mPref.getString (CODE_SECRET, "");
//    }
//
//    @Override
//    public void setCodeSecret ( String codeSecret ) {
//        mPref.edit ().putString (CODE_SECRET, codeSecret).apply ();
//    }

}
