package com.personnalize_design.encore.data;

import android.content.Context;

import com.personnalize_design.encore.data.db.DbHelper;
import com.personnalize_design.encore.data.models.Coupon;
import com.personnalize_design.encore.data.models.ServerResponse;
import com.personnalize_design.encore.data.network.ApiHelper;
import com.personnalize_design.encore.data.preferences.PreferenceHelper;

import javax.inject.Inject;

import io.reactivex.Observable;

public class AppDataManager implements DataManager {

    private final Context mContext;
    private final PreferenceHelper preferenceHelper;
    private final ApiHelper apiHelper;
    private final DbHelper dbHelper;

    @Inject
    public AppDataManager(Context mContext, PreferenceHelper preferenceHelper, ApiHelper apiHelper, DbHelper dbHelper) {
        this.mContext = mContext;
        this.preferenceHelper = preferenceHelper;
        this.apiHelper = apiHelper;
        this.dbHelper = dbHelper;
    }

    public Context getmContext() {
        return mContext;
    }

    public PreferenceHelper getPreferenceHelper() {
        return preferenceHelper;
    }

    public ApiHelper getApiHelper() {
        return apiHelper;
    }

    public DbHelper getDbHelper() {
        return dbHelper;
    }

//    @Override
//    public boolean isUserHaveCoupon ( ) {
//        return preferenceHelper.isUserHaveCoupon ();
//    }
//
//    @Override
//    public void setUserCoupon ( boolean couponValue ) {
//        preferenceHelper.setUserCoupon (couponValue);
//    }
//
//    @Override
//    public boolean isConfirmationCodeValid ( ) {
//        return preferenceHelper.isConfirmationCodeValid ();
//    }
//
//    @Override
//    public void setConfirmationCode ( boolean confirmationValidValue ) {
//        preferenceHelper.setConfirmationCode (confirmationValidValue);
//    }
//
//    @Override
//    public String getConfirmationNumber ( ) {
//        return preferenceHelper.getConfirmationNumber ();
//    }
//
//    @Override
//    public void setConfirmationNumber ( String confirmationNumber ) {
//        preferenceHelper.setConfirmationNumber (confirmationNumber);
//    }
//
//    @Override
//    public String getCodeSecret ( ) {
//        return preferenceHelper.getCodeSecret ();
//    }
//
//    @Override
//    public void setCodeSecret ( String codeSecret ) {
//        preferenceHelper.setCodeSecret (codeSecret);
//    }


//    @Override
//    public Observable<Coupon> reqSaveCouponCode ( String codeValue ) {
//        return apiHelper.reqSaveCouponCode (codeValue);
//    }
//
//    @Override
//    public Observable<ServerResponse> reqConfirmationNumber ( String whatsappNumber ) {
//        return apiHelper.reqConfirmationNumber (whatsappNumber);
//    }
//
//    @Override
//    public Observable<ServerResponse> reqCheckConfirmationNumber ( String confirmationNumber ) {
//        return apiHelper.reqCheckConfirmationNumber (confirmationNumber);
//    }
//
//    @Override
//    public Observable<ServerResponse> reqDeleteSecretCode ( String codeValue ) {
//        return apiHelper.reqDeleteSecretCode (codeValue);
//    }

    @Override
    public Observable<ServerResponse> reqYaourtOrder ( String ouncesValue , String compositionValue , String quantityValue , String deliveryDayValue , String locationPlaceValue , String userPhoneNumberValue, String deliveryPlaceValue, String moreDetailPlaceValue ) {
        return apiHelper.reqYaourtOrder (ouncesValue, compositionValue, quantityValue, deliveryDayValue, locationPlaceValue, userPhoneNumberValue, deliveryPlaceValue, moreDetailPlaceValue);
    }

    @Override
    public Observable<ServerResponse> reqLaitOrder ( String ouncesValue , String compositionValue , String quantityValue , String deliveryDayValue , String locationPlaceValue , String userPhoneNumberValue ) {
        return apiHelper.reqLaitOrder (ouncesValue, compositionValue, quantityValue, deliveryDayValue, locationPlaceValue, userPhoneNumberValue);
    }

    @Override
    public Observable<ServerResponse> reqSellPotOrder ( String locationPlaceValue , String locationPlaceDetail , String userPhoneNumberValue ) {
        return apiHelper.reqSellPotOrder (locationPlaceValue, locationPlaceDetail, userPhoneNumberValue); }
}
