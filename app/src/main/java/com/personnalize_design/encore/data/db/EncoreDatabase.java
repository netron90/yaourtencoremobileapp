package com.personnalize_design.encore.data.db;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.personnalize_design.encore.data.models.Coupon;

@Database (entities = {Coupon.class}, version = 1)
public abstract class EncoreDatabase extends RoomDatabase {

    public abstract CouponDao couponDao();
}
