package com.personnalize_design.encore.data.db;

import javax.inject.Inject;

public class AppDbHelper implements DbHelper {

    private EncoreDatabase db;

    @Inject
    public AppDbHelper(EncoreDatabase db) {
        this.db = db;
    }
}
