package com.personnalize_design.encore.data.models;


import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "coupon")
public class Coupon {

    @PrimaryKey(autoGenerate = true)
    public int id;


    /**
     * response : no exist
     */

    private String response;
    private String code;

    public String getResponse ( ) {
        return response;
    }

    public void setResponse ( String response ) {
        this.response = response;
    }

    public int getId ( ) {
        return id;
    }

    public void setId ( int id ) {
        this.id = id;
    }

    public String getCode ( ) {
        return code;
    }

    public void setCode ( String code ) {
        this.code = code;
    }
}
