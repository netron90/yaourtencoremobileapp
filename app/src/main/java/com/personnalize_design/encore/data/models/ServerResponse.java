package com.personnalize_design.encore.data.models;

public class ServerResponse {


    /**
     * responses : Demande de numero encore envoyer avec succes
     */

    private String responses;

    public String getResponses ( ) {
        return responses;
    }

    public void setResponses ( String responses ) {
        this.responses = responses;
    }
}
