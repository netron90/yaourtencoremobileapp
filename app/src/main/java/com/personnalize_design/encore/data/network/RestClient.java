package com.personnalize_design.encore.data.network;

import com.personnalize_design.encore.data.models.Coupon;
import com.personnalize_design.encore.data.models.ServerResponse;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


import static com.personnalize_design.encore.constants.Mutils.URL_LAIT_ORDER;
import static com.personnalize_design.encore.constants.Mutils.URL_SELL_POT_ORDER;
import static com.personnalize_design.encore.constants.Mutils.URL_YOGURT_ORDER;

public interface RestClient {

//    @FormUrlEncoded
//    @POST(URL_SECRET_CODE)
//    Observable<Coupon> reqGetUser( @Field("code") String codeValue);
//
//    @FormUrlEncoded
//    @POST(URL_CONFIRMATION_NUMBER)
//    Observable<ServerResponse> reqSendConfirmationNumber( @Field("number") String whatsappNumber);
//
//    @FormUrlEncoded
//    @POST(URL_CHECK_NUMBER)
//    Observable<ServerResponse> reqCheckConfirmationNumber( @Field("confirm_number") String confirmNumber);
//
//
//    @FormUrlEncoded
//    @POST(URL_DELETE_SECRET_CODE)
//    Observable<ServerResponse> reqDeleteSecretCode( @Field ("valeurCoupon") String codeValue);

    @FormUrlEncoded
    @POST(URL_YOGURT_ORDER)
    Observable<ServerResponse> reqYogurtOrder( @Field ("ouncesParam") String ounceValue,
                                               @Field ("compositionParam") String compositionValue,
                                               @Field ("quantityParam") String quantityValue,
                                               @Field ("deliveryDayParam") String deliveryDayValue,
                                               @Field ("locationPlaceParam") String locationValue,
                                               @Field ("phoneNumberParam") String phoneNumberValue,
                                               @Field ("deliveryPlaceParam") String deliveryPlaceValue,
                                               @Field ("moreDetailPlaceParam") String moreDetailPlaceValue);

    @FormUrlEncoded
    @POST(URL_LAIT_ORDER)
    Observable<ServerResponse> reqLaitOrder    ( @Field ("ouncesParam") String ounceValue,
                                               @Field ("compositionParam") String compositionValue,
                                               @Field ("quantityParam") String quantityValue,
                                               @Field ("deliveryDayParam") String deliveryDayValue,
                                               @Field ("locationPlaceParam") String locationValue,
                                               @Field ("phoneNumberParam") String phoneNumberValue);

    @FormUrlEncoded
    @POST(URL_SELL_POT_ORDER)
    Observable<ServerResponse> reqSellPotOrder   ( @Field ("locationPlaceParam") String locationValue,
                                                 @Field ("locationPlaceMoreDetailParam") String locationMoreDetailValue,
                                                 @Field ("phoneNumberParam") String phoneNumberValue);
}
