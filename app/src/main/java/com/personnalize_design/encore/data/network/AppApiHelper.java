package com.personnalize_design.encore.data.network;

import com.personnalize_design.encore.constants.Mutils;
import com.personnalize_design.encore.data.models.Coupon;
import com.personnalize_design.encore.data.models.ServerResponse;
import com.personnalize_design.encore.data.preferences.PreferenceHelper;

import javax.inject.Inject;

import io.reactivex.Observable;
import retrofit2.Retrofit;

public class AppApiHelper implements ApiHelper {

    private PreferenceHelper mPref;
    private Retrofit retrofit;
    private Mutils mUtils;


    @Inject
    public AppApiHelper(Retrofit retrofit, PreferenceHelper mPref) {
        this.retrofit = retrofit;
        this.mPref = mPref;
        this.mUtils = new Mutils(this);
    }

    public PreferenceHelper getmPref() {
        return mPref;
    }

    public void setmPref(PreferenceHelper mPref) {
        this.mPref = mPref;
    }

//    @Override
//    public Observable<Coupon> reqSaveCouponCode ( String codeValue ) {
//
//        Observable<Coupon> source = retrofit.create (RestClient.class).reqGetUser (codeValue);
//
//        return source.retryWhen (throwableObservable -> mUtils.retryOnErrorGetData (throwableObservable, source));
//    }
//
//    @Override
//    public Observable<ServerResponse> reqConfirmationNumber ( String whatsappNumber ) {
//        Observable<ServerResponse> source = retrofit.create (RestClient.class).reqSendConfirmationNumber (whatsappNumber);
//
//        return source.retryWhen (throwableObservable -> mUtils.retryOnErrorGetData (throwableObservable, source));
//
//    }
//
//    @Override
//    public Observable<ServerResponse> reqCheckConfirmationNumber ( String confirmationNumber ) {
//        Observable<ServerResponse> source = retrofit.create (RestClient.class).reqCheckConfirmationNumber (confirmationNumber);
//        return source.retryWhen (throwableObservable -> mUtils.retryOnErrorGetData (throwableObservable, source));
//    }
//
//    @Override
//    public Observable<ServerResponse> reqDeleteSecretCode ( String codeValue ) {
//        Observable<ServerResponse> source = retrofit.create (RestClient.class).reqDeleteSecretCode (codeValue);
//        return source.retryWhen (throwableObservable -> mUtils.retryOnErrorGetData (throwableObservable, source));    }

    @Override
    public Observable<ServerResponse> reqYaourtOrder ( String ouncesValue , String compositionValue , String quantityValue , String deliveryDayValue , String locationPlaceValue , String userPhoneNumberValue, String deliveryPlaceValue, String moreDetailPlaceValue ) {
        Observable<ServerResponse> source = retrofit.create (RestClient.class).reqYogurtOrder (ouncesValue, compositionValue, quantityValue, deliveryDayValue, locationPlaceValue, userPhoneNumberValue, deliveryPlaceValue, moreDetailPlaceValue);
        return source.retryWhen (throwableObservable -> mUtils.retryOnErrorGetData (throwableObservable, source));
    }

    @Override
    public Observable<ServerResponse> reqLaitOrder ( String ouncesValue , String compositionValue , String quantityValue , String deliveryDayValue , String locationPlaceValue , String userPhoneNumberValue ) {
        Observable<ServerResponse> source = retrofit.create (RestClient.class).reqLaitOrder (ouncesValue, compositionValue, quantityValue, deliveryDayValue, locationPlaceValue, userPhoneNumberValue);
        return source.retryWhen (throwableObservable -> mUtils.retryOnErrorGetData (throwableObservable, source));
    }

    @Override
    public Observable<ServerResponse> reqSellPotOrder ( String locationPlaceValue , String locationPlaceDetail , String userPhoneNumberValue ) {
        Observable<ServerResponse> source = retrofit.create (RestClient.class).reqSellPotOrder (locationPlaceValue, locationPlaceDetail, userPhoneNumberValue);
        return source.retryWhen (throwableObservable -> mUtils.retryOnErrorGetData (throwableObservable, source));
    }
}
