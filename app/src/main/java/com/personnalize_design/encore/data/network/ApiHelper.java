package com.personnalize_design.encore.data.network;

import com.personnalize_design.encore.data.models.Coupon;
import com.personnalize_design.encore.data.models.ServerResponse;

import io.reactivex.Observable;


public interface ApiHelper {

//    Observable<Coupon> reqSaveCouponCode( String codeValue);
//    Observable<ServerResponse> reqConfirmationNumber(String whatsappNumber);
//    Observable<ServerResponse> reqCheckConfirmationNumber(String confirmationNumber);
//    Observable<ServerResponse> reqDeleteSecretCode(String codeValue);

    Observable<ServerResponse> reqYaourtOrder(String ouncesValue, String compositionValue,
                                              String quantityValue, String deliveryDayValue,
                                              String locationPlaceValue,
                                              String userPhoneNumberValue,
                                              String deliveryPlaceValue,
                                              String moreDetailPlaceValue);

    Observable<ServerResponse> reqLaitOrder(String ouncesValue, String compositionValue,
                                              String quantityValue, String deliveryDayValue,
                                              String locationPlaceValue,
                                              String userPhoneNumberValue);

    Observable<ServerResponse> reqSellPotOrder( String locationPlaceValue,
                                             String locationPlaceDetail,
                                             String userPhoneNumberValue);

}
