package com.personnalize_design.encore.di;


import android.app.Application;
import android.content.Context;

import androidx.room.Room;

import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.personnalize_design.encore.data.AppDataManager;
import com.personnalize_design.encore.data.DataManager;
import com.personnalize_design.encore.data.db.AppDbHelper;
import com.personnalize_design.encore.data.db.DbHelper;
import com.personnalize_design.encore.data.db.EncoreDatabase;
import com.personnalize_design.encore.data.network.ApiHelper;
import com.personnalize_design.encore.data.network.AppApiHelper;
import com.personnalize_design.encore.data.preferences.AppPreferenceHelper;
import com.personnalize_design.encore.data.preferences.PreferenceHelper;
import com.personnalize_design.encore.ui.base.BasePresenter;
import com.personnalize_design.encore.ui.base.MvpPresenter;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.personnalize_design.encore.constants.Mutils.BASE_URL;

@Module
public class ApplicationModule {

    private Application mApplication;

    public ApplicationModule(Application mApplication) {
        this.mApplication = mApplication;
    }

    @Provides
    @Singleton
    public Context provideContext(){
        return this.mApplication.getApplicationContext();
    }


    @Provides
    public MvpPresenter provideBasePresenter(DataManager dataManager, CompositeDisposable compositeDisposable){
        return new BasePresenter(dataManager, compositeDisposable);
    }

    @Provides
    public CompositeDisposable provideCompositeDisposable(){
        return new CompositeDisposable();
    }


    @Provides
    public DataManager provideDataManager(Context context, ApiHelper apiHelper, PreferenceHelper preferenceHelper, DbHelper dbHelper){
        return new AppDataManager(context, preferenceHelper, apiHelper, dbHelper);
    }
//

    @Provides
    public OkHttpClient provideHttpClient(){

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    public Retrofit provideRetrofit(String baseUrl, OkHttpClient client){
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Provides
    public ApiHelper provideApiHelper(){
        return new AppApiHelper(provideRetrofit(BASE_URL, provideHttpClient()), providePreference(provideContext()));
    }

    @Provides
    public PreferenceHelper providePreference(Context context){
        return new AppPreferenceHelper(context);
    }


    @Provides
    @Singleton
    public DbHelper provideAppDbHelper(EncoreDatabase db)
    {
        return new AppDbHelper(db);
    }

    @Provides
    @Singleton
    public EncoreDatabase provideRoomDatabase(Context context){
        return Room.databaseBuilder(context, EncoreDatabase.class, "encore").build();
    }
}
