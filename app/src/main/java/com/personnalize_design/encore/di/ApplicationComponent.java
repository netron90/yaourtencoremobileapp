package com.personnalize_design.encore.di;

import com.personnalize_design.encore.ui.base.BaseActivity;
import com.personnalize_design.encore.ui.final_step.FinalStep;
import com.personnalize_design.encore.ui.final_step.FinalStepMove;
import com.personnalize_design.encore.ui.final_step.FinalStepSell;

import com.personnalize_design.encore.ui.main.MainActivity;
import com.personnalize_design.encore.ui.order.AskAnOrderStart;
import com.personnalize_design.encore.ui.order.LaitCailleOrder;
import com.personnalize_design.encore.ui.order.OrderChoice;
import com.personnalize_design.encore.ui.order.ReducePriceExplanation;
import com.personnalize_design.encore.ui.order.YogurtOrder;

import com.personnalize_design.encore.ui.sell.SellActivityStart;
import com.personnalize_design.encore.ui.sell.SellOrder;
import com.personnalize_design.encore.ui.sell.SellPotExplanation;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {

    void inject( BaseActivity baseActivity);
    void inject( MainActivity mainActivity );
    void inject( AskAnOrderStart askAnOrderStart );
    void inject( SellActivityStart sellActivityStart );
    void inject( ReducePriceExplanation reducePriceExplanation );
    void inject( SellPotExplanation sellPotExplanation );
    void inject( OrderChoice orderChoice );
    void inject( YogurtOrder yogurtOrder );
    void inject( LaitCailleOrder laitCailleOrder );
    void inject( SellOrder sellOrder );
    void inject( FinalStep finalStep );
    void inject( FinalStepMove finalStepMove );
    void inject( FinalStepSell finalStepSell );

}
