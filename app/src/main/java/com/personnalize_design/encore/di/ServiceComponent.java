package com.personnalize_design.encore.di;


import com.personnalize_design.encore.services.CheckUpdate;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component( modules = {ApplicationModule.class})
public interface ServiceComponent {

    void inject ( CheckUpdate checkUpdate );
}
