package com.personnalize_design.encore.constants;

import android.util.Log;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.personnalize_design.encore.data.network.AppApiHelper;

import java.net.SocketTimeoutException;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;

public class Mutils {

    private AppApiHelper mAppApiHelper;

    public Mutils( AppApiHelper mAppApiHelper) {
        this.mAppApiHelper = mAppApiHelper;
    }

    //URL REST API
    public static final String BASE_URL = "https://yaourt-encore.herokuapp.com/";
//    public static final String URL_SECRET_CODE = BASE_URL+"secret_code";
//    public static final String URL_CONFIRMATION_NUMBER = BASE_URL+"whatsapp_number";
//    public static final String URL_CHECK_NUMBER = "check";
//    public static final String URL_DELETE_SECRET_CODE = "delete_coupon";

    public static final String URL_YOGURT_ORDER = "yogurt";
    public static final String URL_LAIT_ORDER = "lait";
    public static final String URL_SELL_POT_ORDER = "sell";


    //PREFERENCE KEY
//    public static final String USER_COUPON = "user_coupon";
//    public static final String CONFIRMATION_CODE_VALID = "confirmation_code_valid";
//    public static final String CONFIRMATION_VALUE = "confirmation_number_value";
//    public static final String CODE_SECRET = "code_secret";




    public ObservableSource<?> retryOnErrorGetData( Observable<Throwable> errors, Observable<?> source){

        return errors.flatMap(error -> {
            Log.d("Error OnErrorAuth", ""+error);
            if(error instanceof HttpException)
            {
                HttpException e = (HttpException) error;
                if(e.code() == 408 || e.code() == 504)
                {

                    Log.d("Error OnErrorAuth", "Error 408 or 504. RetryWhen is executed");
                    return source;
                }
            }else if(error instanceof SocketTimeoutException){
                Log.d("Error OnErrorAuth", "Error SocketTimeOutException. RetryWhen is executed");
                return source;
            }
            Log.d("Error OnErrorAuth", "Error RetryWhen not sucribing again");
            return Observable.error(error);
        });
    }

//    public ObservableSource<List<ParentChild.ElevesBean>> extractListParentChild ( ParentChild parentChild ) {
//        // return Observable.fromIterable ( parentChild.getEleves () );
//        return null;
    }

