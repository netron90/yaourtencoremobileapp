package com.personnalize_design.encore.ui.main;

import android.os.Bundle;
import android.widget.RelativeLayout;

import com.personnalize_design.encore.R;
import com.personnalize_design.encore.ui.base.BaseActivity;
import com.personnalize_design.encore.ui.main.interfaces.MainMvpView;
import com.personnalize_design.encore.ui.main.presenter.MainPresenter;
import com.personnalize_design.encore.ui.order.AskAnOrderStart;
import com.personnalize_design.encore.ui.sell.SellActivityStart;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements MainMvpView {

    @BindView (R.id.askAnOrderStart)
    public RelativeLayout askAnOrderBtn;


    @BindView (R.id.sellCup)
    public RelativeLayout sellCupBtn;


    @Inject
    public MainPresenter<MainActivity> mPresenter;


    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate (savedInstanceState);
        setUp ();
    }

    private void setUp ( ) {
        setContentView (R.layout.activity_main);
        getComponent ().inject (this);
        mPresenter.onAttachView (this);
        setUnbinder (ButterKnife.bind (this));

    }

    @OnClick(R.id.askAnOrderStart)
    public void askAnOrder(){
        startActivity (getContext (), AskAnOrderStart.class);
    }


    @OnClick(R.id.sellCup)
    public void sellCup(){
        startActivity (getContext (), SellActivityStart.class);
    }
}
