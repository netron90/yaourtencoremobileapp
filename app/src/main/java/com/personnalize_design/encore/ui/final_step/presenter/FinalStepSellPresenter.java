package com.personnalize_design.encore.ui.final_step.presenter;

import com.personnalize_design.encore.data.DataManager;
import com.personnalize_design.encore.ui.base.BasePresenter;
import com.personnalize_design.encore.ui.final_step.interfaces.FinalStepSellMvpView;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class FinalStepSellPresenter <V extends FinalStepSellMvpView> extends BasePresenter<V> implements FinalStepSellMvpPresenter<V> {


    @Inject
    public FinalStepSellPresenter ( DataManager dataManager , CompositeDisposable compositeDisposable ) {
        super (dataManager , compositeDisposable);
    }
}
