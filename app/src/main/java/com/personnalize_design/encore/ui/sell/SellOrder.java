package com.personnalize_design.encore.ui.sell;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.personnalize_design.encore.R;
import com.personnalize_design.encore.ui.base.BaseActivity;
import com.personnalize_design.encore.ui.final_step.FinalStep;
import com.personnalize_design.encore.ui.final_step.FinalStepSell;
import com.personnalize_design.encore.ui.sell.interfaces.SellOrderMvpView;
import com.personnalize_design.encore.ui.sell.presenter.SellOrderPresenter;

import org.w3c.dom.Text;

import java.util.concurrent.Callable;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SellOrder extends BaseActivity implements SellOrderMvpView {


    @BindView (R.id.toolbar)
    public Toolbar toolbar;

    @BindView (R.id.spinner_location_place)
    public MaterialSpinner locationPlaceSpinner;

    @BindView (R.id.location_place_desc)
    public EditText locationMoreDetail;

    @BindView (R.id.user_phone_number)
    public EditText userPhoneNumber;

    @BindView (R.id.sellPot)
    public RelativeLayout sellPotBtn;

    @BindView (R.id.order_text)
    public TextView orderText;

    @BindView (R.id.progressBar)
    public ProgressBar progressBar;


    private String locationPlaceValue = "";

    private String locationPlaceMoreDetailValue = "";

    private String userPhoneNumberValue = "";

    @Inject
    SellOrderPresenter<SellOrder> mPresenter;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate (savedInstanceState);
        setUp();
    }

    public void setUp(){
        setContentView (R.layout.activity_sell_order);
        getComponent ().inject (this);
        mPresenter.onAttachView (this);
        setUnbinder (ButterKnife.bind (this));
        setSupportActionBar (toolbar);
        getSupportActionBar ().setTitle (getResources ().getString (R.string.sell_pots));
        getSupportActionBar ().setDisplayShowHomeEnabled (true);
        getSupportActionBar ().setDisplayHomeAsUpEnabled (true);
        locationPlaceValue = getResources().getString(R.string.town_location);
        locationPlaceSpinner.setItems (getResources().getString(R.string.town_location), "Cotonou", "Porto-Novo");
        locationPlaceSpinner.setOnItemSelectedListener (new MaterialSpinner.OnItemSelectedListener<String> () {
            @Override
            public void onItemSelected ( MaterialSpinner view , int position , long id , String item ) {
                locationPlaceValue =item;
            }
        });
    }

    @OnClick(R.id.sellPot)
    public void sellPotBtn(){
        orderText.setVisibility (View.GONE);
        progressBar.setVisibility (View.VISIBLE);
        if(TextUtils.isEmpty (locationMoreDetail.getText ()) || TextUtils.isEmpty (userPhoneNumber.getText ()))
        {
            progressBar.setVisibility (View.GONE);
            orderText.setVisibility (View.VISIBLE);
            showLongToast (getContext (), getString(R.string.empty_field));
        }else
        {
            if(locationPlaceValue.equals(getResources().getString(R.string.town_location)))
            {
                progressBar.setVisibility (View.GONE);
                orderText.setVisibility (View.VISIBLE);
                showLongToast (getContext (), getString(R.string.empty_missed));
            }else{
                showConfirmDiaologBox(SellOrder.this, getString(R.string.confirm_order), getString(R.string.msg_confirm_order), new Callable<Void>() {
                    @Override
                    public Void call() throws Exception {
                        locationPlaceMoreDetailValue = locationMoreDetail.getText().toString();
                        userPhoneNumberValue = userPhoneNumber.getText().toString();
                        mPresenter.sellPotOrder(locationPlaceValue, locationPlaceMoreDetailValue, userPhoneNumberValue);
                      //showLongToast (getContext (), "Composition: " + compositionValue);
                        return null;
                    }
                }, () -> {
                    progressBar.setVisibility (View.GONE);
                    orderText.setVisibility (View.VISIBLE);
                    return null;
                });

            }

        }
    }

    @Override
    public void networkError ( ) {
        progressBar.setVisibility (View.GONE);
        orderText.setVisibility (View.VISIBLE);
        showLongToast (getContext (), getResources ().getString (R.string.error_occured));
    }

    @Override
    public void onSuccessSellOrder ( ) {
        progressBar.setVisibility (View.GONE);
        orderText.setVisibility (View.VISIBLE);
        finish ();
        startActivity (getContext (), FinalStepSell.class);
    }

    @Override
    public void onErrorOccured ( ) {
        progressBar.setVisibility (View.GONE);
        orderText.setVisibility (View.VISIBLE);
        showLongToast (getContext (), getResources ().getString (R.string.error_occured));
    }
}
