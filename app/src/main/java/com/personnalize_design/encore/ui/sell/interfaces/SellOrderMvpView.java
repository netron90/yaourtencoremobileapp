package com.personnalize_design.encore.ui.sell.interfaces;

import com.personnalize_design.encore.ui.base.MvpView;

public interface SellOrderMvpView extends MvpView {
    void networkError ( );

    void onSuccessSellOrder ( );

    void onErrorOccured ( );
}
