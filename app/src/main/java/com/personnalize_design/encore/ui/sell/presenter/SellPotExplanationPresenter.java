package com.personnalize_design.encore.ui.sell.presenter;

import com.personnalize_design.encore.data.DataManager;
import com.personnalize_design.encore.ui.base.BasePresenter;
import com.personnalize_design.encore.ui.sell.interfaces.SellPotExplanationMvpView;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class SellPotExplanationPresenter<V extends SellPotExplanationMvpView> extends BasePresenter<V>  implements SellPotExplanationMvpPresenter<V>{

    @Inject
    public SellPotExplanationPresenter ( DataManager dataManager , CompositeDisposable compositeDisposable ) {
        super (dataManager , compositeDisposable);
    }
}
