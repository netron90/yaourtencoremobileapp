package com.personnalize_design.encore.ui.sell.presenter;

import com.personnalize_design.encore.ui.base.MvpPresenter;
import com.personnalize_design.encore.ui.base.MvpView;

public interface SellActivityStartMvpPresenter <V extends MvpView> extends MvpPresenter<V>{

}