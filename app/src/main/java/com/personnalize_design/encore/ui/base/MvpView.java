package com.personnalize_design.encore.ui.base;

import android.content.Context;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;

import com.personnalize_design.encore.di.ApplicationComponent;

import java.util.concurrent.Callable;

import butterknife.Unbinder;

public interface MvpView {

    void showProgressBar(View v);

    void hideProgressBar( View v);


    void showToast( Context context, String message);

    void showLongToast(Context context, String message);

    void showDiaologBox(String title, String message);

    void showConfirmDiaologBox( Context ctn, String title, String message, Callable<Void> action, Callable<Void> action2 );
    void showConfirmDiaologBoxClosing( String title, String message, Callable<Void> action );

    void showDiaologBoxWithClosingApp(String title, String message);

    void startActivity(Context context, Class destination);

    void changeFragment(Class fragment);

    void changeFragment( Fragment fragment);

    void changeFragment( Fragment fragment, RelativeLayout container);

    void setUnbinder( Unbinder unbinder);

    boolean isNetworkAvailable();

    void onConnectionError();

    ApplicationComponent getComponent();
}
