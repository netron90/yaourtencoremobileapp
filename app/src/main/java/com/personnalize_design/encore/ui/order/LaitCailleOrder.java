package com.personnalize_design.encore.ui.order;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.personnalize_design.encore.R;
import com.personnalize_design.encore.ui.base.BaseActivity;
import com.personnalize_design.encore.ui.final_step.FinalStep;
import com.personnalize_design.encore.ui.order.interfaces.LaitCailleOrderMvpView;
import com.personnalize_design.encore.ui.order.presenter.LaitCailleOrderPresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LaitCailleOrder extends BaseActivity implements LaitCailleOrderMvpView {


    @BindView (R.id.toolbar)
    public Toolbar toolbar;

    @BindView (R.id.spinner_ounce)
    public MaterialSpinner ouncesSpinner;

    @BindView (R.id.spinner_composition)
    public MaterialSpinner compositionSpinner;

    @BindView (R.id.spinner_delivery_day)
    public MaterialSpinner deliveryDaySpinner;

    @BindView (R.id.spinner_location_place)
    public MaterialSpinner locationPlaceSpinner;

    @BindView (R.id.spinner_quantity)
    public EditText spinnerQuantity;

    @BindView (R.id.spinner_numero)
    public EditText spinnerNumero;

    @BindView (R.id.order_lait)
    public RelativeLayout orderYogurtBtn;

    @BindView (R.id.progressBar)
    public ProgressBar progressBar;

    @BindView (R.id.order_text)
    public TextView orderText;

    private String ounceValue = "0,25";

    private String compositionValue = "";

    private String deliveryDayValue = "";

    private String locationPlaceValue = "Porto-Novo";

    private String quantityValue = "";

    private String phoneNumberValue = "";

    @Inject
    LaitCailleOrderPresenter<LaitCailleOrder> mPresenter;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate (savedInstanceState);
        setUp();
    }

    private void setUp ( ) {
        setContentView (R.layout.activity_lait_caille_order);
        getComponent ().inject (this);
        mPresenter.onAttachView (this);
        setUnbinder (ButterKnife.bind (this));
        setSupportActionBar (toolbar);
        getSupportActionBar ().setTitle (getResources ().getString (R.string.yogurt_order_toolbar_title));
        getSupportActionBar ().setDisplayShowHomeEnabled (true);
        getSupportActionBar ().setDisplayHomeAsUpEnabled (true);
        compositionValue = getResources ().getString (R.string.natural_yogurt);
        deliveryDayValue = getResources ().getString (R.string.monday);

        ouncesSpinner.setItems ("0,25L");
        ouncesSpinner.setOnItemSelectedListener (new MaterialSpinner.OnItemSelectedListener<String> () {
            @Override
            public void onItemSelected ( MaterialSpinner view , int position , long id , String item ) {
                ounceValue =item;
            }
        });

        compositionSpinner.setItems (getResources ().getString (R.string.natural_yogurt));
        compositionSpinner.setOnItemSelectedListener (new MaterialSpinner.OnItemSelectedListener<String> () {
            @Override
            public void onItemSelected ( MaterialSpinner view , int position , long id , String item ) {
                compositionValue =item;
            }
        });

        deliveryDaySpinner.setItems (getResources ().getString (R.string.monday), getResources ().getString (R.string.wednesday), getResources ().getString (R.string.friday));
        deliveryDaySpinner.setOnItemSelectedListener (new MaterialSpinner.OnItemSelectedListener<String> () {
            @Override
            public void onItemSelected ( MaterialSpinner view , int position , long id , String item ) {
                deliveryDayValue =item;
            }
        });

        locationPlaceSpinner.setItems ("Porto-Novo");
        locationPlaceSpinner.setOnItemSelectedListener (new MaterialSpinner.OnItemSelectedListener<String> () {
            @Override
            public void onItemSelected ( MaterialSpinner view , int position , long id , String item ) {
                locationPlaceValue =item;
            }
        });
    }

    @OnClick(R.id.order_lait)
    public void orderLait(){
        //TODO: SEND TO OTHER WAY ORDER
        orderText.setVisibility (View.GONE);
        progressBar.setVisibility (View.VISIBLE);
        if(TextUtils.isEmpty (spinnerQuantity.getText ()) || TextUtils.isEmpty (spinnerNumero.getText ()))
        {
            progressBar.setVisibility (View.GONE);
            orderText.setVisibility (View.VISIBLE);
            showLongToast (getContext (), getString(R.string.empty_field));
        }else
        {
            quantityValue = spinnerQuantity.getText ().toString ();
            phoneNumberValue = spinnerNumero.getText ().toString ();
            mPresenter.sendLaitOrder(ounceValue, compositionValue, quantityValue, deliveryDayValue, locationPlaceValue, phoneNumberValue);
        }

    }

    @Override
    public void networkError ( ) {
        progressBar.setVisibility (View.GONE);
        orderText.setVisibility (View.VISIBLE);
        showLongToast (getContext (), getResources ().getString (R.string.connexion_available));
    }

    @Override
    public void onErrorOccured ( ) {
        progressBar.setVisibility (View.GONE);
        orderText.setVisibility (View.VISIBLE);
        showLongToast (getContext (), getResources ().getString (R.string.error_occured));
    }

    @Override
    public void onSuccessLaitOrder ( ) {
        progressBar.setVisibility (View.GONE);
        orderText.setVisibility (View.VISIBLE);
        finish ();
        startActivity (getContext (), FinalStep.class);
    }


}
