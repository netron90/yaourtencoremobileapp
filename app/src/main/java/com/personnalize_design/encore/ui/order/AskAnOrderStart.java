package com.personnalize_design.encore.ui.order;

import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.widget.RelativeLayout;

import com.personnalize_design.encore.R;
import com.personnalize_design.encore.ui.base.BaseActivity;
import com.personnalize_design.encore.ui.order.interfaces.AskAnOrderMvpView;
import com.personnalize_design.encore.ui.order.presenter.AskAnOrderPresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AskAnOrderStart extends BaseActivity implements AskAnOrderMvpView {


    @BindView (R.id.toolbar)
    public Toolbar toolbar;

    @BindView(R.id.askAnOrderBtn)
    public RelativeLayout askAnOrderBtn;


    @BindView (R.id.reduce_explanation_btn)
    public RelativeLayout explanationBtn;

    @Inject
    public AskAnOrderPresenter<AskAnOrderStart> mPresenter;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate (savedInstanceState);
        setUp();
    }

    private void setUp ( ) {
        setContentView (R.layout.activity_ask_an_order);
        getComponent ().inject (this);
        mPresenter.onAttachView (this);
        setUnbinder (ButterKnife.bind (this));
        setSupportActionBar (toolbar);
        getSupportActionBar ().setTitle (getResources ().getString (R.string.make_an_order_toolbar_title));
        getSupportActionBar ().setDisplayShowHomeEnabled (true);
        getSupportActionBar ().setDisplayHomeAsUpEnabled (true);

    }

    @OnClick(R.id.askAnOrderBtn)
    public void askAnOrder(){
        //TODO: SEND TO ASK AN ORDER CHOICE SCREEN
        startActivity (getContext (), YogurtOrder.class);
    }

    @OnClick(R.id.reduce_explanation_btn)
    public void explanationBtn(){
        //TODO: SEND TO REDUCE PRICE PROCESS EXPLANATION
        startActivity (getContext (), ReducePriceExplanation.class);

    }
}
