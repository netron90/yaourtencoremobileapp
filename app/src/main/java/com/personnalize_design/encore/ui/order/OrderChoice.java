package com.personnalize_design.encore.ui.order;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.widget.RelativeLayout;

import com.personnalize_design.encore.R;
import com.personnalize_design.encore.ui.base.BaseActivity;
import com.personnalize_design.encore.ui.order.interfaces.OrderChoiceMvpView;
import com.personnalize_design.encore.ui.order.presenter.OrderChoicePresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderChoice extends BaseActivity implements OrderChoiceMvpView {

    @BindView (R.id.toolbar)
    public Toolbar toolbar;

    @BindView (R.id.yogurt_order)
    public RelativeLayout yogurtOrder;

    @BindView (R.id.lait_caille_order)
    public RelativeLayout laitCailleOrder;

    @Inject
    public OrderChoicePresenter<OrderChoice> mPresenter;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate (savedInstanceState);
        setUp();
    }

    private void setUp ( ) {
        setContentView (R.layout.activity_order_choice);
        getComponent ().inject (this);
        mPresenter.onAttachView (this);
        setUnbinder (ButterKnife.bind (this));
        setSupportActionBar (toolbar);
        getSupportActionBar ().setTitle (getResources ().getString (R.string.make_an_order_choice_toolbar_title));
        getSupportActionBar ().setDisplayShowHomeEnabled (true);
        getSupportActionBar ().setDisplayHomeAsUpEnabled (true);
    }

    @OnClick(R.id.yogurt_order)
    public void orderyogurt(){
        //TODO: SEND TO ORDER YOGURT ACTIVITY
        startActivity (getContext (), YogurtOrder.class);
    }

    @OnClick(R.id.lait_caille_order)
    public void laitCailleOrder(){
        //TODO: SEND TO ORDER LAIT CAILLE ACTIVITY
        startActivity (getContext (), LaitCailleOrder.class);
    }
}
