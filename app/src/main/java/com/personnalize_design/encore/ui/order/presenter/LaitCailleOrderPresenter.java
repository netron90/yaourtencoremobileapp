package com.personnalize_design.encore.ui.order.presenter;

import com.personnalize_design.encore.data.DataManager;
import com.personnalize_design.encore.data.models.ServerResponse;
import com.personnalize_design.encore.ui.base.BasePresenter;
import com.personnalize_design.encore.ui.order.interfaces.LaitCailleOrderMvpView;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LaitCailleOrderPresenter<V extends LaitCailleOrderMvpView> extends BasePresenter<V> implements LaitCailleOrderMvpPresenter<V> {

    private Disposable disposable;
    @Inject
    public LaitCailleOrderPresenter ( DataManager dataManager , CompositeDisposable compositeDisposable ) {
        super (dataManager , compositeDisposable);
    }

    public void sendLaitOrder ( String ounceValue , String compositionValue, String quantityValue, String deliveryDayValue , String locationPlaceValue, String phoneNumberValue ) {
        if(getView () != null)
        {
            if(getView ().isNetworkAvailable ())
            {
                //TODO: SEND LAIT ORDER TO SERVER
                //TODO: SEND YOGURT ORDER TO SERVER
                getDataManager ().reqLaitOrder (ounceValue, compositionValue, quantityValue, deliveryDayValue, locationPlaceValue, phoneNumberValue)
                        .subscribeOn (Schedulers.io ())
                        .observeOn (AndroidSchedulers.mainThread ())
                        .subscribe (new Observer<ServerResponse> () {
                            @Override
                            public void onSubscribe ( Disposable d ) {
                                disposable = d;
                            }

                            @Override
                            public void onNext ( ServerResponse serverResponse ) {
                                if(serverResponse.getResponses ().equals ("Demande envoyer avec succes"))
                                {
                                    if(getView () != null)
                                    {
                                        getView ().onSuccessLaitOrder();
                                    }
                                }else{
                                    if(getView () != null)
                                    {
                                        getView ().onErrorOccured();
                                    }
                                }
                            }

                            @Override
                            public void onError ( Throwable e ) {
                                e.printStackTrace ();
                                if(getView () != null)
                                {
                                    getView ().onErrorOccured();
                                }
                            }

                            @Override
                            public void onComplete ( ) {
                                if(disposable != null)
                                {
                                    disposable.dispose ();
                                    disposable = null;
                                }
                            }
                        });
            }else{
                if(getView () != null)
                {
                    getView ().networkError();
                }
            }
        }
    }
}
