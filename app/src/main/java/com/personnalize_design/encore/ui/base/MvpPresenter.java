package com.personnalize_design.encore.ui.base;

public interface MvpPresenter<V extends MvpView>  {

    void onAttachView(V mvpView);
}
