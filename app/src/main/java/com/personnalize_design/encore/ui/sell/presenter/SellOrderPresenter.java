package com.personnalize_design.encore.ui.sell.presenter;

import com.personnalize_design.encore.data.DataManager;
import com.personnalize_design.encore.data.models.ServerResponse;
import com.personnalize_design.encore.ui.base.BasePresenter;
import com.personnalize_design.encore.ui.sell.interfaces.SellOrderMvpView;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SellOrderPresenter <V extends SellOrderMvpView> extends BasePresenter<V> implements SellOrderMvpPresenter<V> {

    private Disposable disposable;

    @Inject
    public SellOrderPresenter ( DataManager dataManager , CompositeDisposable compositeDisposable ) {
        super (dataManager , compositeDisposable);
    }

    public void sellPotOrder ( String locationPlaceValue , String locationPlaceMoreDetailValue , String userPhoneNumberValue ) {

        if(getView () != null)
        {
            if(getView ().isNetworkAvailable ())
            {
                //TODO: SEND YOGURT ORDER TO SERVER
                getDataManager ().reqSellPotOrder (locationPlaceValue, locationPlaceMoreDetailValue, userPhoneNumberValue)
                        .subscribeOn (Schedulers.io ())
                        .observeOn (AndroidSchedulers.mainThread ())
                        .subscribe (new Observer<ServerResponse> () {
                            @Override
                            public void onSubscribe ( Disposable d ) {
                                disposable = d;
                            }

                            @Override
                            public void onNext ( ServerResponse serverResponse ) {
                                if(serverResponse.getResponses ().equals ("Demande vente envoyer avec succes"))
                                {
                                    if(getView () != null)
                                    {
                                        getView ().onSuccessSellOrder();
                                    }
                                }else{
                                    if(getView () != null)
                                    {
                                        getView ().onErrorOccured();
                                    }
                                }
                            }

                            @Override
                            public void onError ( Throwable e ) {
                                e.printStackTrace ();
                                if(getView () != null)
                                {
                                    getView ().onErrorOccured();
                                }
                            }

                            @Override
                            public void onComplete ( ) {
                                if(disposable != null)
                                {
                                    disposable.dispose ();
                                    disposable = null;
                                }
                            }
                        });

            }else{
                if(getView () != null)
                {
                    getView ().networkError();
                }
            }
        }
    }
}
