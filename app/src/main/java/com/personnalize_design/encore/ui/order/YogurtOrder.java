package com.personnalize_design.encore.ui.order;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jaredrummler.materialspinner.MaterialSpinner;
import com.personnalize_design.encore.R;
import com.personnalize_design.encore.ui.base.BaseActivity;
import com.personnalize_design.encore.ui.final_step.FinalStep;
import com.personnalize_design.encore.ui.final_step.FinalStepMove;

import com.personnalize_design.encore.ui.order.interfaces.YogurtOrderMvpView;
import com.personnalize_design.encore.ui.order.presenter.YogurtOrderPresenter;

import java.util.concurrent.Callable;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class YogurtOrder extends BaseActivity implements YogurtOrderMvpView {

    @BindView (R.id.toolbar)
    public Toolbar toolbar;

    @BindView (R.id.spinner_ounce)
    public MaterialSpinner ouncesSpinner;

    @BindView (R.id.spinner_composition)
    public MaterialSpinner compositionSpinner;

    @BindView (R.id.spinner_delivery_day)
    public MaterialSpinner deliveryDaySpinner;

    @BindView (R.id.spinner_location_place)
    public MaterialSpinner locationPlaceSpinner;

    @BindView (R.id.order_yogurt)
    public RelativeLayout orderYogurtBtn;

    @BindView(R.id.deliveryPlaceBlock)
    public RelativeLayout deliveryPlaceBlock;

    @BindView(R.id.movePlaceOrderBlock)
    public RelativeLayout movePlaceOrderBlock;

    @BindView (R.id.spinner_delivery_place)
    public MaterialSpinner deliveryPlaceSpinner;

    @BindView (R.id.progressBar)
    public ProgressBar progressBar;

    @BindView (R.id.order_text)
    public TextView orderText;

    @BindView (R.id.spinner_quantity)
    public EditText spinnerQuantity;

    @BindView (R.id.spinner_numero)
    public EditText spinnerNumero;

    @BindView(R.id.edittext_move_place)
    public EditText editTextMovePlace;

    @BindView(R.id.img_product_order)
    public ImageView imageProduct;

    private String ounceValue = "0,25";

    private String compositionValue = "";

    private String deliveryDayValue = "";

    private String locationPlaceValue = "";

    private String quantityValue = "";

    private String phoneNumberValue = "";

    private String deliveryPlaceValue = "";

    private String placeMoreDetails = "";

    @Inject
    YogurtOrderPresenter<YogurtOrder> mPresenter;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate (savedInstanceState);
        setUp();
    }

    private void setUp ( ) {
        setContentView (R.layout.activity_yogurt_order);
        getComponent ().inject (this);
        mPresenter.onAttachView (this);
        setUnbinder (ButterKnife.bind (this));
        setSupportActionBar (toolbar);
        getSupportActionBar ().setTitle (getResources ().getString (R.string.yogurt_order_toolbar_title));
        getSupportActionBar ().setDisplayShowHomeEnabled (true);
        getSupportActionBar ().setDisplayHomeAsUpEnabled (true);
        compositionValue = getResources ().getString (R.string.natural_yogurt);
        deliveryDayValue = getResources().getString(R.string.delivery_day_text);
        locationPlaceValue = getResources().getString(R.string.order_location);
        deliveryPlaceValue = getString(R.string.location);

        ouncesSpinner.setItems ("0,25L");
        ouncesSpinner.setOnItemSelectedListener (new MaterialSpinner.OnItemSelectedListener<String> () {
            @Override
            public void onItemSelected ( MaterialSpinner view , int position , long id , String item ) {
                ounceValue =item;
            }
        });

        compositionSpinner.setItems (getResources ().getString (R.string.natural_yogurt), getResources ().getString (R.string.vanilla_yogurt), getResources ().getString (R.string.banana_yogurt), getResources().getString(R.string.lait_caille));
        compositionSpinner.setOnItemSelectedListener (new MaterialSpinner.OnItemSelectedListener<String> () {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onItemSelected ( MaterialSpinner view , int position , long id , String item ) {
                if(item.equals(getResources ().getString (R.string.natural_yogurt)) || item.equals(getResources().getString(R.string.lait_caille)))
                {
                    imageProduct.setImageDrawable(getDrawable(R.drawable.coupon_reduce_price));
                }else if(item.equals(getResources().getString(R.string.vanilla_yogurt)))
                {
                    imageProduct.setImageDrawable(getDrawable(R.drawable.yogurt_vanilla_coupon));
                }else{
                    imageProduct.setImageDrawable(getDrawable(R.drawable.yogurt_fruit_coupon));
                }
                compositionValue = item;
            }
        });

        deliveryDaySpinner.setItems ( getResources().getString(R.string.delivery_day_text), getResources ().getString (R.string.monday), getResources ().getString (R.string.wednesday), getResources ().getString (R.string.friday));
        deliveryDaySpinner.setOnItemSelectedListener (new MaterialSpinner.OnItemSelectedListener<String> () {
            @Override
            public void onItemSelected ( MaterialSpinner view , int position , long id , String item ) {
                deliveryDayValue =item;
            }
        });

        locationPlaceSpinner.setItems (getString(R.string.order_location),getString(R.string.get_order_self), getString(R.string.go_give_order));
        locationPlaceSpinner.setOnItemSelectedListener (new MaterialSpinner.OnItemSelectedListener<String> () {
            @Override
            public void onItemSelected ( MaterialSpinner view , int position , long id , String item ) {

                locationPlaceValue =item;

                if(item.equals(getString(R.string.get_order_self)))
                {
                    deliveryPlaceBlock.setVisibility(View.VISIBLE);
                    deliveryPlaceValue = getResources().getString(R.string.location);
                    movePlaceOrderBlock.setVisibility(View.GONE);
                    placeMoreDetails = "";
                }else if(item.equals(getString(R.string.go_give_order)))
                {
                    deliveryPlaceBlock.setVisibility(View.GONE);
                    movePlaceOrderBlock.setVisibility(View.VISIBLE);
                    deliveryPlaceValue = "";
                    placeMoreDetails = "";
                }
                else{
                    deliveryPlaceBlock.setVisibility(View.GONE);
                    movePlaceOrderBlock.setVisibility(View.GONE);
                    deliveryPlaceValue = "";
                    placeMoreDetails = "";
                }
            }
        });

        deliveryPlaceSpinner.setItems(getResources().getString(R.string.location), "Mont Sinaï (Cotonou)", "BOA GANHI (Cotonou)" ,"Poissonnerie Emmanuel - (Porto-Novo)", "HECM (Porto-Novo)");
        deliveryPlaceSpinner.setOnItemSelectedListener((MaterialSpinner.OnItemSelectedListener<String>) (view, position, id, item) ->
                deliveryPlaceValue = item);
    }

    @OnClick(R.id.order_yogurt)
    public void orderYogurt(){
        //TODO: SEND TO OTHER WAY ORDER
        orderText.setVisibility (View.GONE);
        progressBar.setVisibility (View.VISIBLE);

        if(movePlaceOrderBlock.getVisibility() == View.GONE)
        {
            Log.d("DETAIL CLIENT LOC", "Champ Detail sur l'emplacement du client n'est pas mentionner");

            if(TextUtils.isEmpty (spinnerQuantity.getText ()) || TextUtils.isEmpty (spinnerNumero.getText ()))
            {
                progressBar.setVisibility (View.GONE);
                orderText.setVisibility (View.VISIBLE);
                showLongToast (getContext (), getString(R.string.empty_field));
            }else
            {
                if(deliveryPlaceBlock.getVisibility() == View.GONE)
                {
                    if(deliveryDayValue.equals(getResources().getString(R.string.delivery_day_text)) || locationPlaceValue.equals(getResources().getString(R.string.order_location)))
                    {
                        progressBar.setVisibility (View.GONE);
                        orderText.setVisibility (View.VISIBLE);
                        showLongToast (getContext (), getString(R.string.empty_missed));
                    }else{
                        showConfirmDiaologBox(YogurtOrder.this, getString(R.string.confirm_order), getString(R.string.msg_confirm_order), new Callable<Void>() {
                            @Override
                            public Void call() {
                                quantityValue = spinnerQuantity.getText().toString();
                                phoneNumberValue = spinnerNumero.getText().toString();
                                mPresenter.sendYogurtOrder(ounceValue, compositionValue, quantityValue, deliveryDayValue, locationPlaceValue, phoneNumberValue, deliveryPlaceValue, placeMoreDetails);
//                      showLongToast (getContext (), "Composition: " + compositionValue);
                                return null;
                            }
                        }, () -> {
                            progressBar.setVisibility (View.GONE);
                            orderText.setVisibility (View.VISIBLE);
                            return null;
                        });

                    }
                }else{
                    if(deliveryDayValue.equals(getResources().getString(R.string.delivery_day_text)) || locationPlaceValue.equals(getResources().getString(R.string.order_location)) || deliveryPlaceValue.equals(getResources().getString(R.string.location)))
                    {
                        progressBar.setVisibility (View.GONE);
                        orderText.setVisibility (View.VISIBLE);
                        showLongToast (getContext (), getString(R.string.empty_missed));
                    }else{
                        showConfirmDiaologBox(YogurtOrder.this, getString(R.string.confirm_order), getString(R.string.msg_confirm_order), new Callable<Void>() {
                            @Override
                            public Void call() {
                                quantityValue = spinnerQuantity.getText().toString();
                                phoneNumberValue = spinnerNumero.getText().toString();
                                mPresenter.sendYogurtOrder(ounceValue, compositionValue, quantityValue, deliveryDayValue, locationPlaceValue, phoneNumberValue, deliveryPlaceValue, placeMoreDetails);
//                      showLongToast (getContext (), "Composition: " + compositionValue);
                                return null;
                            }
                        }, () -> {
                            progressBar.setVisibility (View.GONE);
                            orderText.setVisibility (View.VISIBLE);
                            return null;
                        });

                    }
                }


            }

        }else{
            Log.d("DETAIL CLIENT LOC", "Champ Detail sur l'emplacement du client est pas mentionner");

            if(TextUtils.isEmpty (spinnerQuantity.getText ()) || TextUtils.isEmpty (spinnerNumero.getText ()) || TextUtils.isEmpty(editTextMovePlace.getText()))
            {
                progressBar.setVisibility (View.GONE);
                orderText.setVisibility (View.VISIBLE);
                showLongToast (getContext (), getString(R.string.empty_field));
            }else
            {
                if(deliveryPlaceBlock.getVisibility() == View.GONE)
                {
                    if(deliveryDayValue.equals(getResources().getString(R.string.delivery_day_text)) || locationPlaceValue.equals(getResources().getString(R.string.order_location)))
                    {
                        progressBar.setVisibility (View.GONE);
                        orderText.setVisibility (View.VISIBLE);
                        showLongToast (getContext (), getString(R.string.empty_missed));
                    }else{
                        showConfirmDiaologBox(YogurtOrder.this, getString(R.string.confirm_order), getString(R.string.msg_confirm_order), new Callable<Void>() {
                            @Override
                            public Void call() {
                                quantityValue = spinnerQuantity.getText().toString();
                                phoneNumberValue = spinnerNumero.getText().toString();
                                placeMoreDetails = editTextMovePlace.getText().toString();
                                mPresenter.sendYogurtOrder(ounceValue, compositionValue, quantityValue, deliveryDayValue, locationPlaceValue, phoneNumberValue, deliveryPlaceValue, placeMoreDetails);
//                      showLongToast (getContext (), "Composition: " + compositionValue);
                                return null;
                            }
                        }, () -> {
                            progressBar.setVisibility (View.GONE);
                            orderText.setVisibility (View.VISIBLE);
                            return null;
                        });

                    }
                }else{
                    if(deliveryDayValue.equals(getResources().getString(R.string.delivery_day_text)) || locationPlaceValue.equals(getResources().getString(R.string.order_location)))
                    {
                        progressBar.setVisibility (View.GONE);
                        orderText.setVisibility (View.VISIBLE);
                        showLongToast (getContext (), getString(R.string.empty_missed));
                    }else{
                        showConfirmDiaologBox(YogurtOrder.this, getString(R.string.confirm_order), getString(R.string.msg_confirm_order), new Callable<Void>() {
                            @Override
                            public Void call() {
                                quantityValue = spinnerQuantity.getText().toString();
                                phoneNumberValue = spinnerNumero.getText().toString();
                                placeMoreDetails = editTextMovePlace.getText().toString();
                                mPresenter.sendYogurtOrder(ounceValue, compositionValue, quantityValue, deliveryDayValue, locationPlaceValue, phoneNumberValue, deliveryPlaceValue, placeMoreDetails);
//                      showLongToast (getContext (), "Composition: " + compositionValue);
                                return null;
                            }
                        }, () -> {
                            progressBar.setVisibility (View.GONE);
                            orderText.setVisibility (View.VISIBLE);
                            return null;
                        });

                    }
                }


            }
        }


    }

//    @OnClick(R.id.town_not_specified)
//    public void townNotSpecified(){
//        //TODO: SEND TO OTHER WAY ORDER ACTIVITY
//        startActivity (getContext (), NotFoundYogurt.class);
//    }

    @Override
    public void networkError ( ) {
        progressBar.setVisibility (View.GONE);
        orderText.setVisibility (View.VISIBLE);
        showLongToast (getContext (), getResources ().getString (R.string.connexion_available));
    }

    @Override
    public void onErrorOccured ( ) {
        progressBar.setVisibility (View.GONE);
        orderText.setVisibility (View.VISIBLE);
        showLongToast (getContext (), getResources ().getString (R.string.error_occured));
    }

    @Override
    public void onSuccessYogurtOrder ( ) {
        progressBar.setVisibility (View.GONE);
        orderText.setVisibility (View.VISIBLE);
        if(locationPlaceValue.equals(getResources().getString(R.string.get_order_self)))
        {
            finish ();
            startActivity (getContext (), FinalStep.class);
        }else{
            finish ();
            startActivity (getContext (), FinalStepMove.class);
        }

    }
}
