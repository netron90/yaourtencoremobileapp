package com.personnalize_design.encore.ui.order.presenter;

import com.personnalize_design.encore.data.DataManager;
import com.personnalize_design.encore.ui.base.BasePresenter;
import com.personnalize_design.encore.ui.order.interfaces.OrderChoiceMvpView;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class OrderChoicePresenter <V extends OrderChoiceMvpView> extends BasePresenter<V> implements OrderChoiceMvpPresenter<V> {

    @Inject
    public OrderChoicePresenter ( DataManager dataManager , CompositeDisposable compositeDisposable ) {
        super (dataManager , compositeDisposable);
    }
}
