package com.personnalize_design.encore.ui.base;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.personnalize_design.encore.R;
import com.personnalize_design.encore.di.App;
import com.personnalize_design.encore.di.ApplicationComponent;

import java.util.concurrent.Callable;

import butterknife.Unbinder;

public class BaseFragment extends Fragment implements MvpView {

    private ProgressBar progressBar;
    private FragmentManager fragmentManager;
    private Fragment mFragment;
    private Unbinder mUnbinder;


    @Override
    public void showProgressBar(View v) {
        if(v instanceof ProgressBar && v != null)
        {
            v.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgressBar(View v) {
        if(v instanceof ProgressBar && v != null)
        {
            v.setVisibility(View.GONE);
        }
    }

    @Override
    public void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLongToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showDiaologBox(String title, String message) {
        new AlertDialog.Builder(getContext())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", (dialogInterface, i) -> {

                }).show();
    }

    @Override
    public void showConfirmDiaologBox (Context ctn,  String title , String message , Callable<Void> action, Callable<Void> action2 ) {
        new AlertDialog.Builder(getContext())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(getString (R.string.yes_action), (dialogInterface, i) -> {
                    try {
                        action.call ();
                    } catch (Exception e) {
                        e.printStackTrace ();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.no_action), (dialogInterface, i) -> {
                    try {
                        action2.call ();
                    } catch (Exception e) {
                        e.printStackTrace ();
                    }

                }).show();
    }

    @Override
    public void showConfirmDiaologBoxClosing ( String title , String message , Callable<Void> action ) {
        new AlertDialog.Builder(getContext())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", (dialogInterface, i) -> {
                    try {
                        action.call ();
                    } catch (Exception e) {
                        e.printStackTrace ();
                    }
                });

    }

    @Override
    public void showDiaologBoxWithClosingApp(String title, String message) {
        new AlertDialog.Builder(getContext())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", (dialogInterface, i) -> {
                    getActivity().finish();
                }).show();
    }

    @Override
    public void startActivity(Context context, Class destination) {
        Intent intent = new Intent();
        intent.setClass(context, destination);
        startActivity(intent);
    }

    @Override
    public void changeFragment(Class fragment) {

        try {
            mFragment = (Fragment) fragment.newInstance();

//            if(mFragment instanceof LoginFormFragment)
//            {
//                fragmentManager = getFragmentManager();
//                Log.d("BASE FRAGMENT", "Instance of LoginForm");
//                fragmentManager.beginTransaction().addToBackStack(LoginFragment.class.getName())
//                        .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).replace(R.id.container, mFragment).commit();
//            }
//            else {
//                fragmentManager = getFragmentManager();
//                Log.d("BASE FRAGMENT", "not Instance of LoginForm");
//                fragmentManager.beginTransaction().replace(R.id.container, mFragment).commit();
//
//            }

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void changeFragment ( Fragment fragment ) {

        mFragment = fragment;
        fragmentManager = getFragmentManager();
//        fragmentManager.beginTransaction()
//                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).replace(R.id.container_coupon, mFragment).commit();
//        if(mFragment instanceof LoginFormFragment)
//        {
//            Log.d("BASE FRAGMENT", "Instance of LoginForm");
//            fragmentManager.beginTransaction().addToBackStack(LoginFragment.class.getName())
//                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out).replace(R.id.container, mFragment).commit();
//        }
//        else {
//            Log.d("BASE FRAGMENT", "not Instance of LoginForm");
//            fragmentManager.beginTransaction().replace(R.id.container, mFragment).commit();
//
//        }


    }

    @Override
    public void changeFragment ( Fragment fragment, RelativeLayout container ) {
//        if(container.getVisibility () == View.VISIBLE)
//        {
//            mFragment = fragment;
//            fragmentManager = getFragmentManager();
//            fragmentManager.beginTransaction().replace(R.id.container, mFragment).commit();
//        }else{
//            mFragment = fragment;
//            fragmentManager = getFragmentManager();
//            fragmentManager.beginTransaction().replace(R.id.container_normal, mFragment).commit();
//        }

    }

    @Override
    public void setUnbinder(Unbinder unbinder) {
        this.mUnbinder = unbinder;
    }

    @Override
    public boolean isNetworkAvailable() {

        ConnectivityManager connectivityManager = (ConnectivityManager)getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    @Override
    public void onConnectionError() {
        showLongToast(getContext(), getResources().getString(R.string.connexion_available));
    }

    @Override
    public ApplicationComponent getComponent() {
        return ((App)getActivity().getApplication()).getmComponent() ;
    }
}
