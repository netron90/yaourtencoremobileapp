package com.personnalize_design.encore.ui.order;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.widget.RelativeLayout;

import com.personnalize_design.encore.R;
import com.personnalize_design.encore.ui.base.BaseActivity;
import com.personnalize_design.encore.ui.order.interfaces.ReducePriceExplanationMvpView;
import com.personnalize_design.encore.ui.order.presenter.ReducePriceExplanationPresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReducePriceExplanation extends BaseActivity implements ReducePriceExplanationMvpView {

    @BindView (R.id.toolbar)
    public Toolbar toolbar;

    @BindView (R.id.askAnOrderStart)
    public RelativeLayout aksAnOrderBtn;

    @Inject
    public ReducePriceExplanationPresenter<ReducePriceExplanation> mPresenter;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate (savedInstanceState);
        setUp();
    }

    private void setUp ( ) {
        setContentView (R.layout.activity_reduceprice_explanation);
        getComponent ().inject (this);
        mPresenter.onAttachView (this);
        setUnbinder (ButterKnife.bind (this));
        setSupportActionBar (toolbar);
        getSupportActionBar ().setTitle (getResources ().getString (R.string.make_an_order_process_toolbar_title));
        getSupportActionBar ().setDisplayShowHomeEnabled (true);
        getSupportActionBar ().setDisplayHomeAsUpEnabled (true);
    }

    @OnClick(R.id.askAnOrderStart)
    public void askAnOrder(){
        //TODO: SEND TO ORDER CHOICE SCREEN
        finish ();
        startActivity (getContext (), YogurtOrder.class);
    }
}
