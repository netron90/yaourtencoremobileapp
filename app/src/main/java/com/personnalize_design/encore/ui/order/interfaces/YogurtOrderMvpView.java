package com.personnalize_design.encore.ui.order.interfaces;

import com.personnalize_design.encore.ui.base.MvpView;

public interface YogurtOrderMvpView extends MvpView {
    void networkError ( );

    void onErrorOccured ( );

    void onSuccessYogurtOrder ( );
}
