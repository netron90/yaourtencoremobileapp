package com.personnalize_design.encore.ui.final_step.presenter;

import com.personnalize_design.encore.data.DataManager;
import com.personnalize_design.encore.ui.base.BasePresenter;
import com.personnalize_design.encore.ui.final_step.interfaces.FinalStepMvpView;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class FinalStepPresenter<V extends FinalStepMvpView> extends BasePresenter<V> implements FinalStepMvpPresenter<V> {

    @Inject
    public FinalStepPresenter ( DataManager dataManager , CompositeDisposable compositeDisposable ) {
        super (dataManager , compositeDisposable);
    }
}
