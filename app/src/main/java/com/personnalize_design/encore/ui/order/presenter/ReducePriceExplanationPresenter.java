package com.personnalize_design.encore.ui.order.presenter;

import com.personnalize_design.encore.data.DataManager;
import com.personnalize_design.encore.ui.base.BasePresenter;
import com.personnalize_design.encore.ui.order.interfaces.ReducePriceExplanationMvpView;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class ReducePriceExplanationPresenter <V extends ReducePriceExplanationMvpView> extends BasePresenter<V> implements ReducePriceExplanationMvpPresenter<V> {

    @Inject
    public ReducePriceExplanationPresenter ( DataManager dataManager , CompositeDisposable compositeDisposable ) {
        super (dataManager , compositeDisposable);
    }
}
