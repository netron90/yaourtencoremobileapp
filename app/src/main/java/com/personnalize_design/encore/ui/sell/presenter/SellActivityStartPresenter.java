package com.personnalize_design.encore.ui.sell.presenter;

import com.personnalize_design.encore.data.DataManager;
import com.personnalize_design.encore.ui.base.BasePresenter;
import com.personnalize_design.encore.ui.sell.interfaces.SellActivityStartMvpView;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class SellActivityStartPresenter <V extends SellActivityStartMvpView> extends BasePresenter<V> implements SellActivityStartMvpPresenter<V> {

    @Inject
    public SellActivityStartPresenter ( DataManager dataManager , CompositeDisposable compositeDisposable ) {
        super (dataManager , compositeDisposable);
    }
}
