package com.personnalize_design.encore.ui.sell;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.widget.RelativeLayout;

import com.personnalize_design.encore.R;
import com.personnalize_design.encore.ui.base.BaseActivity;
import com.personnalize_design.encore.ui.sell.interfaces.SellPotExplanationMvpView;
import com.personnalize_design.encore.ui.sell.presenter.SellPotExplanationPresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SellPotExplanation extends BaseActivity implements SellPotExplanationMvpView {

    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    @BindView (R.id.sellCup)
    public RelativeLayout sellPotBtn;

    @Inject
    public SellPotExplanationPresenter<SellPotExplanation> mPresenter;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate (savedInstanceState);
        setUp ();
    }

    private void setUp ( ) {
        setContentView (R.layout.activity_sell_pot_explanation);
        getComponent ().inject (this);
        mPresenter.onAttachView (this);
        setUnbinder (ButterKnife.bind (this));
        setSupportActionBar (toolbar);
        getSupportActionBar ().setTitle (getResources ().getString (R.string.sell_pot_process_toolbar_title));
        getSupportActionBar ().setDisplayShowHomeEnabled (true);
        getSupportActionBar ().setDisplayHomeAsUpEnabled (true);
    }

    @OnClick(R.id.sellCup)
    public void setSellPotBtn(){
        //TODO: SEND TO SELL POT SCREEN
        startActivity (getContext (), SellOrder.class);
    }

}
