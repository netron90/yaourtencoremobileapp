package com.personnalize_design.encore.ui.order.presenter;

import com.personnalize_design.encore.ui.base.MvpPresenter;
import com.personnalize_design.encore.ui.base.MvpView;

public interface OrderChoiceMvpPresenter<V extends MvpView> extends MvpPresenter<V> {
}
