package com.personnalize_design.encore.ui.order.presenter;

import com.personnalize_design.encore.data.DataManager;
import com.personnalize_design.encore.ui.base.BasePresenter;
import com.personnalize_design.encore.ui.order.interfaces.AskAnOrderMvpView;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class AskAnOrderPresenter<V extends AskAnOrderMvpView> extends BasePresenter<V> implements AskAnOrderMvpPresenter<V> {

    @Inject
    public AskAnOrderPresenter ( DataManager dataManager , CompositeDisposable compositeDisposable ) {
        super (dataManager , compositeDisposable);
    }
}
