package com.personnalize_design.encore.ui.final_step;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.widget.RelativeLayout;

import com.personnalize_design.encore.R;
import com.personnalize_design.encore.ui.base.BaseActivity;
import com.personnalize_design.encore.ui.final_step.interfaces.FinalStepMoveMvp;
import com.personnalize_design.encore.ui.final_step.presenter.FinalStepMovePresenter;
import com.personnalize_design.encore.ui.main.MainActivity;

import javax.inject.Inject;

public class FinalStepMove extends BaseActivity implements FinalStepMoveMvp {

    @Inject
    FinalStepMovePresenter<FinalStepMove> mPresenter;

    @BindView(R.id.final_step_ok_btn)
    public RelativeLayout finalStepBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUp();
    }

    private void setUp ( ) {
        setContentView(R.layout.activity_final_step_move);
        getComponent ().inject (this);
        mPresenter.onAttachView (this);
        setUnbinder (ButterKnife.bind (this));

    }

    @OnClick(R.id.final_step_ok_btn)
    public void finalStepOkBtn(){
        finish ();
        Intent intent = new Intent (getContext (), MainActivity.class);
        intent.addFlags (Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity (intent);
    }
}
