package com.personnalize_design.encore.ui.base;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AlertDialogLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.personnalize_design.encore.R;
import com.personnalize_design.encore.di.App;
import com.personnalize_design.encore.di.ApplicationComponent;

import java.util.concurrent.Callable;

import javax.inject.Inject;

import butterknife.Unbinder;

public class BaseActivity extends AppCompatActivity implements MvpView {


    private ProgressBar progressBar;
    private FragmentManager fragmentManager;
    private Fragment mFragment;
    private Unbinder mUnbinder;


    @Inject
    public Context context;

    @Override
    public void onCreate ( Bundle savedInstanceState , PersistableBundle persistentState ) {
        super.onCreate (savedInstanceState , persistentState);

        ((App)getApplication()).getmComponent().inject(this);
    }

    @Override
    public void showProgressBar ( View v ) {
        if(v instanceof ProgressBar && v != null)
        {
            v.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideProgressBar ( View v ) {
        if(v instanceof ProgressBar && v != null)
        {
            v.setVisibility(View.GONE);
        }
    }

    @Override
    public void showToast ( Context context , String message ) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLongToast ( Context context , String message ) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showDiaologBox ( String title , String message ) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", (dialogInterface, i) -> {

                }).show();
    }

    @Override
    public void showConfirmDiaologBox ( Context ctn, String title , String message , Callable<Void> action, Callable<Void> action2 ) {

                new AlertDialog.Builder(ctn)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(getString(R.string.yes_action), ( dialogInterface, i) -> {
                    try {
                        action.call ();
                    } catch (Exception e) {
                        e.printStackTrace ();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.no_action), (dialogInterface, i) -> {
                    try {
                        action2.call ();
                    } catch (Exception e) {
                        e.printStackTrace ();
                    }

                }).show();
    }

    @Override
    public void showConfirmDiaologBoxClosing ( String title , String message , Callable<Void> action ) {
        new AlertDialog.Builder(getContext())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", (dialogInterface, i) -> {
                    try {
                        action.call ();
                    } catch (Exception e) {
                        e.printStackTrace ();
                    }
                });
    }


    @Override
    public void showDiaologBoxWithClosingApp ( String title , String message ) {
        new AlertDialog.Builder(getContext())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", (dialogInterface, i) -> {
                    finish();
                }).show();
    }

    @Override
    public void startActivity ( Context context , Class destination ) {
        Intent intent = new Intent(context, destination);
        startActivity (intent);
    }

    @Override
    public void changeFragment ( Class fragment ) {
        try {
            mFragment = (Fragment) fragment.newInstance();
            fragmentManager = getSupportFragmentManager();
//            fragmentManager.beginTransaction().replace(R.id.container_coupon, mFragment).commit();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void changeFragment ( Fragment fragment ) {
        mFragment = fragment;
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.container, mFragment).commit();
    }

    @Override
    public void changeFragment ( Fragment fragment , RelativeLayout container ) {
//        if(container.getVisibility () == View.VISIBLE)
//        {
//            mFragment = fragment;
//            fragmentManager = getSupportFragmentManager();
//            fragmentManager.beginTransaction().replace(R.id.container, mFragment).commit();
//        }else{
//            mFragment = fragment;
//            fragmentManager = getSupportFragmentManager();
//            fragmentManager.beginTransaction().replace(R.id.container_normal, mFragment).commit();
//        }
    }

    @Override
    public void setUnbinder ( Unbinder unbinder ) {
        this.mUnbinder = unbinder;
    }

    @Override
    public boolean isNetworkAvailable ( ) {
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    @Override
    public void onConnectionError ( ) {
        showLongToast(getContext(), getString(R.string.connexion_available));
    }

    @Override
    public ApplicationComponent getComponent ( ) {
        return ((App)getApplication()).getmComponent();
    }

    public Context getContext(){
        return this.context;
    }

}
