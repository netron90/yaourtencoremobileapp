package com.personnalize_design.encore.ui.main.presenter;

import com.personnalize_design.encore.ui.base.MvpPresenter;
import com.personnalize_design.encore.ui.base.MvpView;

public interface MainMvpPresenter<V extends MvpView> extends MvpPresenter<V> {
}
