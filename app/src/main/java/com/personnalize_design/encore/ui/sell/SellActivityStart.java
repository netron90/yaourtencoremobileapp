package com.personnalize_design.encore.ui.sell;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.widget.RelativeLayout;

import com.personnalize_design.encore.R;
import com.personnalize_design.encore.ui.base.BaseActivity;
import com.personnalize_design.encore.ui.sell.interfaces.SellActivityStartMvpView;
import com.personnalize_design.encore.ui.sell.presenter.SellActivityStartMvpPresenter;
import com.personnalize_design.encore.ui.sell.presenter.SellActivityStartPresenter;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SellActivityStart extends BaseActivity implements SellActivityStartMvpView {

    @BindView (R.id.toolbar)
    public Toolbar toolbar;

    @BindView (R.id.sell_pot_Btn)
    public RelativeLayout sellPotBtn;

    @BindView (R.id.sell_pot_explanation_btn)
    public RelativeLayout sellPotExplanationBtn;

    @Inject
    public SellActivityStartPresenter<SellActivityStart> mPresenter;

    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate (savedInstanceState);
        setUp();
    }

    private void setUp ( ) {
        setContentView (R.layout.activity_sell_start);
        getComponent ().inject (this);
        mPresenter.onAttachView (this);
        setUnbinder (ButterKnife.bind (this));
        setSupportActionBar (toolbar);
        getSupportActionBar ().setTitle (getResources ().getString (R.string.sell_pot_toolbar_title));
        getSupportActionBar ().setDisplayShowHomeEnabled (true);
        getSupportActionBar ().setDisplayHomeAsUpEnabled (true);
    }

    @OnClick(R.id.sell_pot_Btn)
    public void sellPotStartProcess(){
        //TODO: SEND TO SELL POT SCREEN PROCESS BEGIN
        startActivity (getContext (), SellOrder.class);
    }

    @OnClick(R.id.sell_pot_explanation_btn)
    public void sellPotExplanation(){
        //TODO: SEND TO SELL PROCESS EXPLANATION
        startActivity (getContext (), SellPotExplanation.class);
    }
}
