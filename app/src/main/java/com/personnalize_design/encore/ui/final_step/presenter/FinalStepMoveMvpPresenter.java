package com.personnalize_design.encore.ui.final_step.presenter;

import com.personnalize_design.encore.ui.base.MvpPresenter;
import com.personnalize_design.encore.ui.base.MvpView;

public interface FinalStepMoveMvpPresenter<V extends MvpView> extends MvpPresenter<V> {
}
