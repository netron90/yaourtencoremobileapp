package com.personnalize_design.encore.ui.main.presenter;

import com.personnalize_design.encore.data.DataManager;
import com.personnalize_design.encore.ui.base.BasePresenter;
import com.personnalize_design.encore.ui.main.interfaces.MainMvpView;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;


public class MainPresenter<V extends MainMvpView> extends BasePresenter<V> implements MainMvpPresenter<V> {

    @Inject
    public MainPresenter ( DataManager dataManager , CompositeDisposable compositeDisposable ) {
        super (dataManager , compositeDisposable);
    }

}
